package com.epam.sms;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class SmsSender {
    private static final String ACCOUNT_SID = "ACd91b7874f177ea4dd657cfa176351950";
    private static final String AUTH_TOKEN = "f6d69f212ba44083b4e1091d34ea88ac";

    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message.creator(new PhoneNumber("+380971484443"), new PhoneNumber("+17576343442"), str).create();
    }
}
